package com.example.sample.mvvm.fragment_screens.favs.di

import com.example.sample.mvvm.di.PerFragmentScope
import com.example.sample.mvvm.fragment_screens.favs.FavsFragment
import dagger.Subcomponent
import dagger.android.AndroidInjector

@PerFragmentScope
@Subcomponent(modules = [(FavsModule::class)])
interface FavsSubComponent : AndroidInjector<FavsFragment> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<FavsFragment>()
}