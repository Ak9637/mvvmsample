package com.example.sample.mvvm.fragment_screens.users

import androidx.lifecycle.LiveData
import com.example.sample.mvvm.common.BaseContracts
import com.example.sample.mvvm.data.models.dataModels.User

class UsersContracts{


    companion object {
        const val TAG="UsersContracts"

    }

    interface ViewModel : BaseContracts.ViewModel {

        fun getUsers(): LiveData<List<User>>

    }
    interface View: BaseContracts.View{



    }

}