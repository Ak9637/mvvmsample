package com.example.sample.mvvm.common

import android.app.Activity
import android.view.View
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.sample.mvvm.BuildConfig
import com.example.sample.mvvm.R
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast

/**
 * All fragments must extend this class
 * as it contains the common methods which is required by all fragments
 * LIke a central loading view, toast and alerts
 */

open class BaseFragment : Fragment(){
    companion object {
        const val TAG="BaseFragment"
    }

    fun showShortToast(message:String){
        if(activity!=null && !activity!!.isFinishing){
            if(activity is BaseActivity){
                (activity as BaseActivity).showMessage(message)
            }else{
                Toast.makeText(activity?.applicationContext,message, Toast.LENGTH_SHORT).show()

            }
        }


    }
    fun showLongToast(message:String){
        if(activity!=null && !activity!!.isFinishing){
            if(activity is BaseActivity){
                (activity as BaseActivity).showMessage(message)
            }else{
                Toast.makeText(activity?.applicationContext,message, Toast.LENGTH_LONG).show()

            }
        }

    }

    fun showShortToast(messageStringRes:Int){
        if(activity!=null && !activity!!.isFinishing){
            if(activity is BaseActivity){
                (activity as BaseActivity).showMessage(activity!!.resources.getString(messageStringRes))
            }else{
                Toast.makeText(activity?.applicationContext,activity!!.resources.getString(messageStringRes), Toast.LENGTH_SHORT).show()

            }
        }

    }
    fun showLongToast(messageStringRes:Int){
        if(activity!=null && !activity!!.isFinishing){
            if(activity is BaseActivity){
                (activity as BaseActivity).showMessage(activity!!.resources.getString(messageStringRes))
            }else{
                Toast.makeText(activity?.applicationContext,activity!!.resources.getString(messageStringRes), Toast.LENGTH_LONG).show()

            }
        }
    }

    fun showDebugShortToast(message:String){
        if(BuildConfig.DEBUG){
            Toast.makeText(activity?.applicationContext,message, Toast.LENGTH_SHORT).show()
        }

    }
    fun showDebugLongToast(message:String){
        if(BuildConfig.DEBUG){
            Toast.makeText(activity?.applicationContext,message, Toast.LENGTH_LONG).show()
        }

    }

    fun showDebugShortToast(messageStringRes:Int){
        if(BuildConfig.DEBUG){
            Toast.makeText(activity?.applicationContext,activity!!.resources.getString(messageStringRes), Toast.LENGTH_SHORT).show()

        }

    }
    fun showDebugLongToast(messageStringRes:Int){
        if(BuildConfig.DEBUG){
            Toast.makeText(activity?.applicationContext,activity!!.resources.getString(messageStringRes), Toast.LENGTH_LONG).show()
        }

    }


    fun showError(error:String?){
        if(error==null || error.isEmpty()){
            showShortToast(getString(R.string.somethingWentWrong))
        }else{
            showShortToast(error)
        }
    }
    fun showFailure(failure:String?){
        if(failure==null || failure.isEmpty()){
            showShortToast(getString(R.string.somethingWentWrong))
        }else{
            showShortToast(failure)
        }
    }

    fun showLoading(){
        if(activity!=null){
            (activity as BaseActivity).showLoading()
        }
    }
    fun dismissLoading(){
        if(activity!=null){
            (activity as BaseActivity).dismissLoading()
        }
    }

    override fun onDestroyView() {
        if(activity!=null){
            if(activity is Activity){
                //activity?.setActionBar(null)

            }else if(activity is AppCompatActivity){
                //(activity as AppCompatActivity)?.setSupportActionBar(null)

            }
        }

        super.onDestroyView()


    }





}