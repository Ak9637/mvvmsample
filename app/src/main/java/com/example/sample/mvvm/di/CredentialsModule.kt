package com.example.sample.mvvm.di


import com.example.sample.mvvm.BuildConfig
import com.example.sample.mvvm.root.Constants
import dagger.Module
import dagger.Provides
import javax.inject.Named

/**
 *Module providing the Credential for transcations
 */
@Module
class CredentialsModule{

    @Provides
    @Named(Constants.API_BASE)
    @MyAppScope
    fun getBaseUrl(): String {
        return BuildConfig.SERVER_URL
    }

}