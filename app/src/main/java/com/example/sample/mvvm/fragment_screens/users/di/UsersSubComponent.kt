package com.example.sample.mvvm.fragment_screens.users.di

import com.example.sample.mvvm.di.PerActivityScope
import com.example.sample.mvvm.di.PerFragmentScope
import com.example.sample.mvvm.fragment_screens.users.UsersFragment
import dagger.Subcomponent
import dagger.android.AndroidInjector

@PerFragmentScope
@Subcomponent(modules = [(UsersModule::class)])
interface UsersSubComponent : AndroidInjector<UsersFragment> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<UsersFragment>()
}