package com.example.sample.mvvm.screens.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import com.example.sample.mvvm.R
import com.example.sample.mvvm.common.BaseActivity
import com.google.android.material.tabs.TabLayout
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.ref.WeakReference
import javax.inject.Inject
import javax.inject.Named
import androidx.navigation.Navigation
import androidx.navigation.NavController
import org.jetbrains.anko.toast


class MainActivity :BaseActivity(),MainContracts.View {
    companion object{
        const val TAG="MainAct"
    }


    @Inject
    @field: Named(MainContracts.TAG)
    lateinit var viewModel: MainContracts.ViewModel

    lateinit var navController:NavController


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        setNewContentView(R.layout.activity_main)
        inits()


    }

    private fun inits(){
        navController=  Navigation.findNavController(this@MainActivity, R.id.my_main_nav_host_fragment)


        viewModel.getUsers().observe(this, Observer {
            Log.d(MainContracts.TAG,"Users loaded :"+it.size)
        })

        mainTabLayout.addOnTabSelectedListener(object:TabLayout.BaseOnTabSelectedListener<TabLayout.Tab>{
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if(tab!=null)
                {
                    loadPage(tab.position)
                }

            }
        })

    }

    private fun loadPage(position: Int) {
        if(position==0){
            //load users list
            navController.navigate(R.id.action_favsFragment_to_usersFragment)

        }else if(position==1){
            //load fav list
            navController.navigate(R.id.action_usersFragment_to_favsFragment)


        }


        //showMessage("current stack count is :"+my_main_nav_host_fragment?.childFragmentManager?.backStackEntryCount);

    }

    private fun loadUsers(){

    }


}
