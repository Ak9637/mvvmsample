package com.example.sample.mvvm.common

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast

/**
 * Loads a full page dialog
 */


open class
BaseFullScreenDialogFragment: DialogFragment(){

    override fun onStart() {
        super.onStart()


        if (dialog != null) {
            dialog!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog!!.window!!.setLayout(width, height)
        }
    }

    fun showShortToast(message:String){
        context?.toast(message)
    }
    fun showLongToast(message:String){
        context?.longToast(message)
    }

    fun showShortToast(messageStringRes:Int){
        context?.toast(context!!.resources.getString(messageStringRes))
    }
    fun showLongToast(messageStringRes:Int){
        context?.longToast(context!!.resources.getString(messageStringRes))
    }


}