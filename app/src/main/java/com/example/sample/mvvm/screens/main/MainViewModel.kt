package com.example.sample.mvvm.screens.main

import androidx.lifecycle.LiveData
import com.example.sample.mvvm.common.BaseViewModel
import com.example.sample.mvvm.data.Repository
import com.example.sample.mvvm.data.models.dataModels.User


class MainViewModel(private val repo: Repository) : BaseViewModel(), MainContracts.ViewModel {
    override fun getUsers(): LiveData<List<User>> {
        return  repo.getUsers()
    }

}
