package com.example.sample.mvvm.fragment_screens.favs

import androidx.lifecycle.LiveData
import com.example.sample.mvvm.common.BaseContracts
import com.example.sample.mvvm.data.models.dataModels.User

class FavsContracts{


    companion object {
        const val TAG="FavsContracts"

    }

    interface ViewModel : BaseContracts.ViewModel {
        fun getUsers(): LiveData<List<User>>

    }
    interface View: BaseContracts.View{



    }

}