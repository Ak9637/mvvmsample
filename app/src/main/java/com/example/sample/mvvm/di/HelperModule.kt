package com.example.sample.mvvm.di

import com.example.sample.mvvm.di.MyAppScope
import com.google.gson.Gson
import dagger.Module
import dagger.Provides


/**
 *Module providing the General Helper dependencies
 */
@Module
class HelperModule {

    companion object {
    }




    @Provides
    @MyAppScope
    fun getGson(): Gson {
        return Gson()
    }


}
