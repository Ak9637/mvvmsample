package com.example.sample.mvvm.extensions

import android.util.TypedValue
import android.widget.TextView

fun TextView.clearText(){
    text=""
}
fun TextView.setDensityIndepentText(text:String,dimension:Float){
    setText(text)
    setTextSize(
        TypedValue.COMPLEX_UNIT_SP,
        dimension / context.resources.displayMetrics.density)
}
fun TextView.setDensityIndepentTextSize(dimension:Float){
    setTextSize(
        TypedValue.COMPLEX_UNIT_SP,
        dimension / context.resources.displayMetrics.density)
}
fun TextView.setMyBackgroundColor(resId:Int){
    setBackgroundColor(context.getMyColor(resId))
}
fun TextView.setMyTextColor(resId:Int){
    setTextColor(context.getMyColor(resId))
}