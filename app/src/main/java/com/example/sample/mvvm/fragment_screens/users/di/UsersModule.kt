package com.example.sample.mvvm.fragment_screens.users.di

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.sample.mvvm.common.MyViewModelFactory
import com.example.sample.mvvm.data.Repository
import com.example.sample.mvvm.fragment_screens.users.UsersContracts
import com.example.sample.mvvm.fragment_screens.users.UsersFragment
import com.example.sample.mvvm.fragment_screens.users.UsersViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class UsersModule {

    @Provides
    @Named(UsersContracts.TAG)
    fun provideUsersViewModel(fragment:UsersFragment, @Named(UsersContracts.TAG) viewModelProvider: ViewModelProvider.Factory): UsersContracts.ViewModel {
        return ViewModelProviders.of(fragment, viewModelProvider).get(UsersViewModel::class.java)
    }

    @Provides
    @Named(UsersContracts.TAG)
    fun provideUsersViewModelFactory(repo: Repository): ViewModelProvider.Factory {
        return MyViewModelFactory(UsersViewModel(repo))
    }
}