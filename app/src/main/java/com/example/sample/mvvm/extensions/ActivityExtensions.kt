package com.example.sample.mvvm.extensions

import android.app.Activity
import android.graphics.drawable.Drawable
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import com.example.sample.mvvm.BuildConfig

fun Activity.debugShort(message: String) {
    if (BuildConfig.DEBUG) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}

fun Activity.debugLong(message: String) {
    if (BuildConfig.DEBUG) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}

fun Activity.shortT(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Activity.longT(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Activity.getMyColor(resId:Int): Int {
    return ResourcesCompat.getColor(this.resources,resId,null)
}
fun Activity.getMyDrawable(resId:Int): Drawable? {
    return ResourcesCompat.getDrawable(this.resources,resId,null)
}
fun Activity.getMyInteger(resId:Int): Int? {
    return this.resources.getInteger(resId)
}
fun Activity.getMyDimen(resId:Int): Float {
    return this.resources.getDimension(resId)
}fun Activity.getMyIntDimen(resId:Int): Int {
    return this.resources.getDimension(resId).toInt()
}
