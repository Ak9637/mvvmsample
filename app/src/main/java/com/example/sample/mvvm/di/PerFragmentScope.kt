package com.example.sample.mvvm.di

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import javax.inject.Scope


/**
 *Interface restricting scope of dependencies to per fragment lifecycle
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
annotation class PerFragmentScope