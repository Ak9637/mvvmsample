package com.example.sample.mvvm.screens.main

import androidx.lifecycle.LiveData
import com.example.sample.mvvm.common.BaseContracts
import com.example.sample.mvvm.data.models.dataModels.User
import com.google.gson.JsonObject


class MainContracts{


    companion object {
        const val TAG="MainContracts"

    }

    interface ViewModel : BaseContracts.ViewModel {

        fun getUsers():LiveData<List<User>>

    }
    interface View:BaseContracts.View{



    }

}