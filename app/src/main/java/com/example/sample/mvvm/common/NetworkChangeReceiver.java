package com.example.sample.mvvm.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Listen to changes in network state
 */

public class NetworkChangeReceiver extends BroadcastReceiver {

    private PublishSubject<Boolean> publishSubject = PublishSubject.create();

    @Override
    public void onReceive(final Context context, final Intent intent) {

        int status = NetworkUtil.getConnectivityStatusString(context);
        publishSubject.onNext(status == NetworkUtil.NETWORK_STATUS_NOT_CONNECTED);

    }

    public Observable<Boolean> getNetworkChangeObservable() {
        return publishSubject.asObservable();
    }
}