package com.example.sample.mvvm.screens.main.di

import com.example.sample.mvvm.di.PerActivityScope
import com.example.sample.mvvm.screens.main.MainActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

@PerActivityScope
@Subcomponent(modules = [MainFragmentsBuilder::class,MainModule::class])
interface MainSubComponent : AndroidInjector<MainActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MainActivity>()
}