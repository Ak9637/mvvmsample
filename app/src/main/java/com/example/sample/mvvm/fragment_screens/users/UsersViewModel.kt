package com.example.sample.mvvm.fragment_screens.users

import androidx.lifecycle.LiveData
import com.example.sample.mvvm.common.BaseViewModel
import com.example.sample.mvvm.data.Repository
import com.example.sample.mvvm.data.models.dataModels.User

class UsersViewModel(private val repo: Repository) : BaseViewModel(), UsersContracts.ViewModel {
    override fun getUsers(): LiveData<List<User>> {
        return  repo.getUsers()
    }

}

