package com.example.sample.mvvm.di

import com.example.sample.mvvm.data.ApiService
import com.example.sample.mvvm.data.Repository
import com.example.sample.mvvm.data.RepositoryImplementation
import com.example.sample.mvvm.data.local.SecureSharedPreferences
import com.example.sample.mvvm.helpers.SharedPreferencesManager
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named


/**
 *Module providing data repositories dependencies
 */
@Module(includes = arrayOf(RetrofitModule::class))
class RepositoryModule{

    @Provides
    @MyAppScope
    fun getApiService(retrofit:Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    @MyAppScope
    fun getApiRepository(
        apiService: ApiService,
        securedPref: SecureSharedPreferences,
        @Named(DataModule.APP_PREFERENCES) appPref: SharedPreferencesManager
    ): Repository {
        return RepositoryImplementation(apiService,securedPref,appPref)
    }


}