package com.example.sample.mvvm.di

import android.app.Application
import android.content.Context
import com.example.sample.mvvm.root.ThisApplication
import com.example.sample.mvvm.screens.main.di.MainSubComponent
import dagger.Module
import dagger.Provides


/**
 * App Module
 * All Sub-dependencies , i.e all independent dependencies like activities , singletons ,etc modules are added here
 * for access to APP level dependencies
 */

@Module(
    subcomponents = [
    MainSubComponent::class
    ]
)


class AppModule {
    @Provides
    @MyAppScope
    internal fun provideContext(application: ThisApplication): Context {
        return application.applicationContext
    }

    @Provides
    @MyAppScope
    internal fun provideApplication(application: ThisApplication): Application {
        return application
    }
}