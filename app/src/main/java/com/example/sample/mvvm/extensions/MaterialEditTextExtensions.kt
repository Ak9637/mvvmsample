package com.homingos.phostagram.extensions

import com.rengwuxian.materialedittext.MaterialEditText

fun MaterialEditText.getString():String{
    return if(this.text==null){
        ""
    }else{
        this.text.toString()
    }
}
fun MaterialEditText.setNewError(error:String?){
    if(error==null || error.isEmpty()){
        this.setError(null)
    }else{
        this.setError(error)
    }
}
fun MaterialEditText.setNewError(res:Int){
    this.setNewError(this.context.getString(res))
}
fun MaterialEditText.clearError(){
    this.setError(null)
}
