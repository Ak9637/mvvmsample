package com.example.sample.mvvm.data

import androidx.lifecycle.LiveData
import com.example.sample.mvvm.data.models.dataModels.User
import com.google.gson.JsonObject


interface Repository{

    fun getUsers(): LiveData<List<User>>
}