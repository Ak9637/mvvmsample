package com.example.sample.mvvm.extensions

import android.animation.Animator
import android.content.Context
import android.view.View
import android.view.animation.Interpolator
import android.view.inputmethod.InputMethodManager
import com.example.sample.mvvm.root.Constants
import com.example.sample.mvvm.root.ThisApplication
import com.jakewharton.rxbinding.view.RxView
import com.jakewharton.rxbinding.view.RxView.*
import rx.android.schedulers.AndroidSchedulers
import rx.functions.Action1
import java.util.concurrent.TimeUnit

fun View.turnGone(){
    if(this.visibility!=View.GONE)
    this.visibility=View.GONE
}

fun View.turnInvisible(){
    if(this.visibility!=View.INVISIBLE)
    this.visibility=View.INVISIBLE
}

fun View.turnVisible(){
    if(this.visibility!=View.VISIBLE)
    this.visibility=View.VISIBLE
}

fun View.isVisible():Boolean{
    return this.visibility==View.VISIBLE
}
fun View.isGone():Boolean{
    return this.visibility==View.GONE
}
fun View.isInvisible():Boolean{
    return this.visibility==View.INVISIBLE
}
fun View.showSoftKeyboard(){
    (ThisApplication.mInstance.applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

}
fun View.hideSoftKeyboard(){
    (ThisApplication.mInstance.applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(this.windowToken, 0);

}
fun View.fadeOut(duration:Long,interpolator:Interpolator?,listener:Animator.AnimatorListener?){
    turnVisible()
    val animation = animate().alpha(0f).setDuration(duration)
    if(listener!=null){
        animation.setListener(listener)
    }else{
        animation.setListener(object:Animator.AnimatorListener{
            override fun onAnimationRepeat(animation: Animator?) {
            }

            override fun onAnimationEnd(animation: Animator?) {
                turnGone()
            }

            override fun onAnimationCancel(animation: Animator?) {
                turnGone()
            }

            override fun onAnimationStart(animation: Animator?) {
            }
        })
    }
    if(interpolator!=null){
        animation.interpolator = interpolator
    }
    animation.start()
}
fun View.fadeIn(duration:Long,interpolator:Interpolator?,listener:Animator.AnimatorListener?){
    turnVisible()
    val animation = animate().alpha(1f).setDuration(duration)
    if(listener!=null){
        animation.setListener(listener)
    }
    if(interpolator!=null){
        animation.interpolator = interpolator
    }
    animation.start()
}
fun View.setOnDefaultThrottledClickListener(action: Action1<Any>)  {
    /**
     * Throttle multiple events during given duration and emits only first action
     */
    try{
        clicks(this).throttleFirst(Constants.BUTTON_CLICK_THROTTLE_TIME, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(action, Action1 {
                //prints error
                it.printStackTrace()
            })
    }catch (e:Exception){
        e.printStackTrace()
    }


}