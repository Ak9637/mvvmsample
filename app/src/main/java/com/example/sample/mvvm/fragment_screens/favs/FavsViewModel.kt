package com.example.sample.mvvm.fragment_screens.favs

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.sample.mvvm.common.BaseViewModel
import com.example.sample.mvvm.data.Repository
import com.example.sample.mvvm.data.models.dataModels.User
import java.util.function.Function

class FavsViewModel(private val repo: Repository) : BaseViewModel(), FavsContracts.ViewModel {
    override fun getUsers(): LiveData<List<User>> {
        return  repo.getUsers()
    }

}

