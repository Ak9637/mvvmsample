package com.example.sample.mvvm.screens.main.di

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.sample.mvvm.common.MyViewModelFactory
import com.example.sample.mvvm.data.Repository
import com.example.sample.mvvm.fragment_screens.favs.di.FavsSubComponent
import com.example.sample.mvvm.fragment_screens.users.di.UsersSubComponent
import com.example.sample.mvvm.screens.main.MainActivity
import com.example.sample.mvvm.screens.main.MainContracts
import com.example.sample.mvvm.screens.main.MainViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Named


@Module(subcomponents = [UsersSubComponent::class,FavsSubComponent::class])
class MainModule {

    @Provides
    @Named(MainContracts.TAG)
    fun provideMainViewModel(activity: MainActivity, @Named(MainContracts.TAG) viewModelProvider: ViewModelProvider.Factory): MainContracts.ViewModel {
        return ViewModelProviders.of(activity, viewModelProvider).get(MainViewModel::class.java)
    }

    @Provides
    @Named(MainContracts.TAG)
    fun provideMainViewModelFactory(repo: Repository): ViewModelProvider.Factory {
        return MyViewModelFactory(MainViewModel(repo))
    }
}