package com.example.sample.mvvm.root

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND
import android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE
import android.app.Application
import android.content.*
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.multidex.BuildConfig
import androidx.multidex.MultiDex
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.example.sample.mvvm.di.DaggerApplicationComponent
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.firebase.FirebaseApp
import com.squareup.leakcanary.LeakCanary
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit


import javax.inject.Inject
import io.fabric.sdk.android.Fabric
import io.fabric.sdk.android.InitializationCallback
import java.lang.Exception
import java.lang.ref.WeakReference

/**
 * Application class
 * This class is responsible for listening to internet connection and user session status and
 * Initialse all App level requirements
 */

class ThisApplication: Application(), HasActivityInjector {



    companion object {
        const val APP_TAG = "Homingos"
        @Volatile
        lateinit var mInstance: ThisApplication

    }






    private val unCaughtExceptionHandler = Thread.UncaughtExceptionHandler { d, e ->
        run {
            // catches and prints any unhandled exception in entire app
            e.printStackTrace()
        }
    }





    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>












    override fun onCreate() {

        mInstance = this


        DaggerApplicationComponent.builder().injectApplication(this).build().inject(this)
        //MultiDex.install(this)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        inits()
        initLibraries()

//       val data=AppSignatureHelper(this).appSignatures
//        var code = ""
//        for( value in data){
//           code= "$code $value"
//
//        }
//
//        Toast.makeText(this, "code is :$code",Toast.LENGTH_LONG).show()

        super.onCreate()


    }

    private fun inits() {




    }

    private fun initLibraries() {
        Thread.setDefaultUncaughtExceptionHandler(unCaughtExceptionHandler)

//        FirebaseApp.initializeApp(this)
//
//       // Fabric.with(this, Crashlytics())
//
//
//
//        val  core =CrashlyticsCore.Builder()
//            .build();
//        Fabric.with(Fabric.Builder(this).kits(Crashlytics.Builder()
//            .core(core)
//            .build())
//            .debuggable(true)
//            .initializationCallback(object:InitializationCallback<Fabric> {
//                override fun success(p0: Fabric?) {
//                    p0?.toString()
//                }
//
//                override fun failure(p0: Exception?) {
//                    p0?.message.toString()
//                }
//            })
//            .build());

//        if(!BuildConfig.FLAVOR.equals("uat")){
//
//
//                    if (LeakCanary.isInAnalyzerProcess(this)) {
//            // This process is dedicated to LeakCanary for heap analysis.
//            // You should not init your app in this process.
//            return
//        }
//
//            val excludedRefs = AndroidExcludedRefs.createAppDefaults()
//                .instanceField("com.yanzhenjie.*", "*")
//                .instanceField("android.*", "*")
//                .build()
//
//            LeakCanary.refWatcher(this)
//                .watchDelay(10,TimeUnit.SECONDS)
//                .excludedRefs(excludedRefs)
//                .buildAndInstall()
//        }

 //       Fresco.initialize(this)
    }

    @SuppressLint("CheckResult")
    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        Log.d(APP_TAG, level.toString() + "")
        System.gc()

        /**
         * If system reaches criticical memory states , clear space
         */

        io.reactivex.Observable.just(level)
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .doOnError { e -> e.printStackTrace() }
            .subscribe { level ->
                run {
                    if (level <= ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW) {
                        // Fresco.getImagePipeline().clearCaches()
                        // Fresco.getImagePipeline().clearDiskCaches()
                    }

                }
            }

    }


    override fun activityInjector(): AndroidInjector<Activity>? {
        return dispatchingAndroidInjector
    }



}