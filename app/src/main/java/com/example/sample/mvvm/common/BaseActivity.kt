package com.example.sample.mvvm.common

import android.app.ActivityManager
import android.content.*
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.transition.ChangeBounds
import android.transition.TransitionManager
import android.util.Log
import android.view.*
import android.widget.FrameLayout
import android.widget.Toast
import androidx.annotation.VisibleForTesting
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.test.espresso.IdlingResource
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject
import android.widget.TextView
import android.widget.LinearLayout
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.abangfadli.shotwatch.ShotWatch
import com.example.sample.mvvm.BuildConfig
import com.example.sample.mvvm.R
import com.example.sample.mvvm.extensions.turnGone
import com.example.sample.mvvm.extensions.turnVisible
import kotlinx.android.synthetic.main.activity_base.*
import java.lang.ref.WeakReference


/**
 * All activities must extend this class
 * as it contains the common methods which is required by all activities
 * LIke a central loading view, toast and alerts
 */

open class BaseActivity : FragmentActivity(), HasSupportFragmentInjector{

    private lateinit var baseProgressFrame : FrameLayout
    private lateinit var baseLayoutFrame : FrameLayout
    private lateinit var customToastMessageTv : TextView
    private lateinit var customToastRootLL : LinearLayout
    private lateinit var toast : Toast




    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        // SET SCREEN ORIENTATION TO POTRAIT
        if (Build.VERSION.SDK_INT == 26) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
        } else {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }


//        window.allowEnterTransitionOverlap = true
//        window.allowReturnTransitionOverlap = true
//        window.allowEnterTransitionOverlap =true

        setContentView(R.layout.activity_base)
        baseProgressFrame = findViewById(R.id.baseProgress)
        baseLayoutFrame = findViewById(R.id.baseFrame)



        /**
         *Loads all Vectors properly in back support flag
         */
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

       // IMMLeaks.fixFocusedViewLeak(application);

        inits()


    }



    fun setNewContentView(layoutResID: Int) {



        baseLayoutFrame.addView(layoutInflater.inflate(layoutResID,null))

        val customToastLayout=layoutInflater.inflate(R.layout.layout_custom_toast,null,false)

        customToastMessageTv= customToastLayout.findViewById(R.id.custom_toast_tv)
        customToastRootLL= customToastLayout.findViewById(R.id.custom_toast_layout_root)


        toast.setGravity(Gravity.TOP or Gravity.FILL_HORIZONTAL, 0, 0)
        toast.view = customToastLayout



    }


    private fun inits() {

        /**
         * Custom toast is created
         */

        toast = Toast(applicationContext)






    }


    private fun displayShortToast(){
        toast.duration=Toast.LENGTH_SHORT

         try{
             if(!toast.view.isShown){
                 toast.show()
             }     // true if visible
        } catch (e:Exception) {         // invisible if exception
            toast.show()
       }

    }
    private fun displayLongToast(){
        toast.duration=Toast.LENGTH_LONG

        try{
            if(!toast.view.isShown){
                toast.show()
            }     // true if visible
        } catch (e:Exception) {         // invisible if exception
            toast.show()
        }

    }

    fun showError(error:String?){
        if(error==null || error.isEmpty()){
            customToastMessageTv.text = this.resources.getString(R.string.somethingWentWrong)
        }else{
            customToastMessageTv.text = error

        }
        displayShortToast()
    }
    fun showMessage(message:String?){
        if(message==null || message.isEmpty()){
            customToastMessageTv.text = this.resources.getString(R.string.somethingWentWrong)
        }else{
            customToastMessageTv.text = message

        }
        displayShortToast()
    }
    fun showFailure(failure:String?){
        if(failure==null || failure.isEmpty()){
            customToastMessageTv.text = this.resources.getString(R.string.somethingWentWrong)
        }else{
            customToastMessageTv.text = failure

        }
        displayShortToast()
    }


    fun showLongError(error:String?){
        if(error==null || error.isEmpty()){
            customToastMessageTv.text = this.resources.getString(R.string.somethingWentWrong)
        }else{
            customToastMessageTv.text = error

        }
        displayLongToast()
    }
    fun showLongMessage(message:String?){
        if(message==null || message.isEmpty()){
            customToastMessageTv.text = this.resources.getString(R.string.somethingWentWrong)
        }else{
            customToastMessageTv.text = message

        }
        displayLongToast()
    }
    fun showLongFailure(failure:String?){
        if(failure==null || failure.isEmpty()){
            customToastMessageTv.text = this.resources.getString(R.string.somethingWentWrong)
        }else{
            customToastMessageTv.text = failure

        }
        displayLongToast()
    }

    fun showDebugShortToast(message:String){
        if(BuildConfig.DEBUG){
            Toast.makeText(applicationContext,message,Toast.LENGTH_SHORT).show()
        }

    }
    fun showDebugLongToast(message:String){
        if(BuildConfig.DEBUG){
            Toast.makeText(applicationContext,message,Toast.LENGTH_LONG).show()
        }

    }

    fun showDebugShortToast(messageStringRes:Int){
        if(BuildConfig.DEBUG){
            Toast.makeText(applicationContext,this!!.resources.getString(messageStringRes),Toast.LENGTH_SHORT).show()

        }

    }
    fun showDebugLongToast(messageStringRes:Int){
        if(BuildConfig.DEBUG){
            Toast.makeText(applicationContext,this!!.resources.getString(messageStringRes),Toast.LENGTH_LONG).show()
        }

    }

    override fun onResume() {


        super.onResume()


    }


    fun showLoading(){
        baseProgressFrame?.turnVisible()

    }
    fun dismissLoading(){
        baseProgressFrame?.turnGone()

    }

    override fun onDestroy() {
        baseProgressFrame?.removeAllViews()
        baseFrame?.removeAllViews()


        super.onDestroy()

    }




    override fun onPause() {


        super.onPause()


    }





}