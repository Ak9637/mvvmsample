package com.example.sample.mvvm.data

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.sample.mvvm.data.local.SecureSharedPreferences
import com.example.sample.mvvm.data.models.dataModels.User
import com.example.sample.mvvm.helpers.SharedPreferencesManager
import com.google.gson.JsonObject
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import rx.Subscriber


class RepositoryImplementation(
    val apiService: ApiService,
    val securedPrefs: SecureSharedPreferences,
    val appPref: SharedPreferencesManager
) : Repository {
    val users= ArrayList<User>()

    val usersLiveData= MutableLiveData<ArrayList<User>>()


    @SuppressLint("CheckResult")
    override fun getUsers(): LiveData<List<User>>{

        if(users.isEmpty()){
            apiService.getUsers()
                .retry(3)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object:Observer<Response<ArrayList<User>>>{
                    override fun onComplete() {
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(t: Response<ArrayList<User>>) {
                        users.addAll(t.body()!!)
                        usersLiveData.value = users
                    }

                    override fun onError(e: Throwable) {
                    }
                })
        }


        return usersLiveData as LiveData<List<User>>

    }



}