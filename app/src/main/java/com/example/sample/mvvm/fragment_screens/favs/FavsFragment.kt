package com.example.sample.mvvm.fragment_screens.favs

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.sample.mvvm.R
import com.example.sample.mvvm.adapter.UsersAdapter
import com.example.sample.mvvm.common.BaseFragment
import com.example.sample.mvvm.screens.main.MainContracts
import com.example.sample.mvvm.screens.map.MapsActivity
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.favs_fragment.*
import javax.inject.Inject
import javax.inject.Named


class FavsFragment : BaseFragment() ,FavsContracts.View{


    companion object {
        const val TAG="FavsFragment"
    }

    @Inject
    @field: Named(FavsContracts.TAG)
    lateinit var viewModel: FavsContracts.ViewModel

    lateinit var usersAdapter : UsersAdapter




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.favs_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        inits()
    }

    private fun inits() {
        viewModel.getUsers().observe(this, Observer {
            Log.d(MainContracts.TAG,"Users loaded :"+it.size)
            val newData=it.filter {user->
                user.fav
            }
            usersAdapter = UsersAdapter(newData)
            favsRv.adapter = usersAdapter

            usersAdapter.getClickListener().observe(this@FavsFragment, Observer {user->
                MapsActivity.launchForUser(this@FavsFragment.activity!!,user)
            })
        })
    }

    private fun initViews() {
        favsRv.layoutManager = LinearLayoutManager(this.context)


    }

    override fun onDestroy() {

        super.onDestroy()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)

        super.onAttach(context)
        processArgument(arguments)


    }

    override fun onResume() {
        super.onResume()

    }

    private fun processArgument(arguments: Bundle?) {
//        if(arguments!=null && arguments.containsKey(FavsContracts.TASK_NAME)){
//            setMode(arguments.getInt(FavsContracts.TASK_NAME))
//
//            if(mode== FavsContracts.MODE.SELF_EDIT){
//                userInfo = arguments.getParcelable<UserInfo>(FavsContracts.TASK_DATA)
//
//            }else if(mode== FavsContracts.MODE.CONTACT_EDIT){
//                contactInfo = arguments.getParcelable<ContactInfo>(FavsContracts.TASK_DATA)
//
//            }
//        }


    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

    }





    override fun onActivityCreated(savedInstanceState: Bundle?) {

        super.onActivityCreated(savedInstanceState)

    }



}

