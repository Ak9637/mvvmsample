package com.example.sample.mvvm.fragment_screens.favs.di

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.sample.mvvm.common.MyViewModelFactory
import com.example.sample.mvvm.data.Repository
import com.example.sample.mvvm.fragment_screens.favs.FavsContracts
import com.example.sample.mvvm.fragment_screens.favs.FavsFragment
import com.example.sample.mvvm.fragment_screens.favs.FavsViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class FavsModule {

    @Provides
    @Named(FavsContracts.TAG)
    fun provideFavsViewModel(fragment: FavsFragment, @Named(FavsContracts.TAG) viewModelProvider: ViewModelProvider.Factory): FavsContracts.ViewModel {
        return ViewModelProviders.of(fragment, viewModelProvider).get(FavsViewModel::class.java)
    }

    @Provides
    @Named(FavsContracts.TAG)
    fun provideFavsViewModelFactory(repo: Repository): ViewModelProvider.Factory {
        return MyViewModelFactory(FavsViewModel(repo))
    }
}