package com.example.sample.mvvm.screens.main.di

import androidx.fragment.app.Fragment
import com.example.sample.mvvm.fragment_screens.favs.FavsFragment
import com.example.sample.mvvm.fragment_screens.favs.di.FavsSubComponent
import com.example.sample.mvvm.fragment_screens.users.UsersFragment
import com.example.sample.mvvm.fragment_screens.users.di.UsersSubComponent
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap

@Module
abstract class MainFragmentsBuilder{

    @Binds
    @IntoMap
    @FragmentKey(UsersFragment::class)
    abstract fun bindUsersFragmentInjectorFactory(builder: UsersSubComponent.Builder): AndroidInjector.Factory<out Fragment>

    @Binds
    @IntoMap
    @FragmentKey(FavsFragment::class)
    abstract fun bindFavsFragmentInjectorFactory(builder:FavsSubComponent.Builder): AndroidInjector.Factory<out Fragment>




}