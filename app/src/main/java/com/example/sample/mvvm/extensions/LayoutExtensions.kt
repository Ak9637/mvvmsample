package com.example.sample.mvvm.extensions


import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.example.sample.mvvm.BuildConfig
import com.google.android.material.snackbar.Snackbar.*

fun CoordinatorLayout.showShortSnackbar(message:String) = make(this,message, LENGTH_SHORT).show()
fun CoordinatorLayout.showLongSnackbar(message:String) = make(this,message, LENGTH_LONG).show()

fun CoordinatorLayout.showShortDebugSnackbar(message:String) {
    if(BuildConfig.DEBUG) make(this,message, LENGTH_SHORT).show()
}

fun CoordinatorLayout.showLongDebugSnackbar(message:String) {
    if(BuildConfig.DEBUG) make(this,message, LENGTH_LONG).show()
}
