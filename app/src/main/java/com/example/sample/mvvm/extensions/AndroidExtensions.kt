package com.example.sample.mvvm.extensions

import android.app.Dialog
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import java.lang.ref.WeakReference

fun Drawable.setTintColor(color:Int){
    colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.MULTIPLY)
}

fun DialogFragment.showSingleInstance(activity: WeakReference<AppCompatActivity>, dialogFragment: WeakReference<DialogFragment>, tag:String){
    val ft=activity.get()?.supportFragmentManager?.beginTransaction()
    val existingFragment= activity.get()?.supportFragmentManager?.findFragmentByTag(tag)
    if (existingFragment== null) {
        // same fragment does not exists
        dialogFragment.get()?.show(ft!!, tag)
        ft?.commit()

    }
}

fun Dialog.showIfNotAlreadyShowing(){
    if(!isShowing){
        show()
    }
}