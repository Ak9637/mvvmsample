package com.example.sample.mvvm.common

import androidx.fragment.app.Fragment
import java.lang.Exception
/**
 * Abstraction of fragment transactions
 * If activities uses fragments then extend this class
 */


open abstract class BaseFragmentActivity : BaseActivity() {

    private val ACTIVITY_FINISHING_EXCEPTION =
        Exception("Activity is finishing,hence fragment transaction is suspended")

    fun addFragment(
        frame: Int,
        fragment: Fragment,
        tag: String,
        allowStateLoss: Boolean,
        shouldNotBeAlreadyAdded: Boolean
    ): Boolean {
        return try {
            if (this.isFinishing) {
                throw ACTIVITY_FINISHING_EXCEPTION
            } else {
                if (shouldNotBeAlreadyAdded && !isFragmentAlreadyAdded(tag)) {
                    if (allowStateLoss) {
                        supportFragmentManager
                            .beginTransaction()
                            .add(frame, fragment, tag)
                            .commitAllowingStateLoss()
                    } else {
                        supportFragmentManager
                            .beginTransaction()
                            .add(frame, fragment, tag)
                            .commit()
                    }
                } else {
                    //Bring the added fragment to front
                    if (getAlreadyAddedFragment(tag) != null) {
                        replaceFragment(frame, getAlreadyAddedFragment(tag)!!, tag, allowStateLoss)

                    }

                }



                true
            }

        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    fun replaceFragment(
        frame: Int,
        fragment: Fragment,
        tag: String,
        allowStateLoss: Boolean
    ): Boolean {
        return try {
            if (this.isFinishing) {
                throw ACTIVITY_FINISHING_EXCEPTION
            } else {

                if (allowStateLoss) {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(frame, fragment, tag)
                        .commitAllowingStateLoss()
                } else {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(frame, fragment, tag)
                        .commit()
                }


                true
            }

        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    fun addFragmentAndPushToBackStack(
        frame: Int,
        fragment: Fragment,
        tag: String,
        allowStateLoss: Boolean,
        shouldNotBeAlreadyAdded: Boolean
    ): Boolean {
        return try {
            if (this.isFinishing) {
                throw ACTIVITY_FINISHING_EXCEPTION
            } else {
                if (shouldNotBeAlreadyAdded && !isFragmentAlreadyAdded(tag)) {
                    if (allowStateLoss) {
                        supportFragmentManager
                            .beginTransaction()
                            .add(frame, fragment, tag)
                            .addToBackStack(tag)
                            .commitAllowingStateLoss()
                    } else {
                        supportFragmentManager
                            .beginTransaction()
                            .add(frame, fragment, tag)
                            .addToBackStack(tag)
                            .commit()
                    }
                } else {
                    //Bring the added fragment to front
                    if (getAlreadyAddedFragment(tag) != null) {
                        replaceFragmentAndPushToBackStack(frame, getAlreadyAddedFragment(tag)!!, tag, allowStateLoss)

                    }

                }



                true
            }

        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    fun replaceFragmentAndPushToBackStack(
        frame: Int,
        fragment: Fragment,
        tag: String,
        allowStateLoss: Boolean
    ): Boolean {
        return try {
            if (this.isFinishing) {
                throw ACTIVITY_FINISHING_EXCEPTION
            } else {

                if (allowStateLoss) {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(frame, fragment, tag)
                        .addToBackStack(tag)
                        .commitAllowingStateLoss()
                } else {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(frame, fragment, tag)
                        .addToBackStack(tag)
                        .commit()
                }


                true
            }

        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }


    fun isFragmentAlreadyAdded(tag: String): Boolean {
        return supportFragmentManager.findFragmentByTag(tag) != null
    }

    fun getAlreadyAddedFragment(tag: String): Fragment? {
        return supportFragmentManager.findFragmentByTag(tag)
    }

    fun popBackStackImmediate() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStackImmediate()
        }
    }



    fun clearBackStack() {
        while (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        }
    }
//
//    override fun onBackPressed() {
//        super.onBackPressed()
////        if (supportFragmentManager.backStackEntryCount <= 1) {
////            onBackPressedOverEmptyBackStackRequested()
////        } else {
////            supportFragmentManager.popBackStack()
////        }
//    }

   // abstract fun onBackPressedOverEmptyBackStackRequested()

}