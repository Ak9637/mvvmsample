package com.example.sample.mvvm.screens.map

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.sample.mvvm.R
import com.example.sample.mvvm.common.BaseActivity
import com.example.sample.mvvm.data.models.dataModels.User

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsActivity : BaseActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap


    companion object{
        fun launchForUser(activity: Activity, user: User){
            val intent = Intent(activity,MapsActivity::class.java)
            intent.putExtra("user",user)
            activity?.startActivity(intent)
        }
    }

    private var currentUser:User?=null



    private fun inits(){

    }

    private fun loadData(){

    }
    private fun processIntent() {
        currentUser = intent.extras.getParcelable("user")
        loadData()
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setNewContentView(R.layout.activity_maps)


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        inits()
        processIntent()
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val marker = LatLng(currentUser!!.address!!.geo!!.lat!!.toDouble(), currentUser!!.address!!.geo!!.lat!!.toDouble())
        mMap.addMarker(MarkerOptions().position(marker).title("${currentUser!!.address!!.getAddress()}"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(marker))
    }
}
