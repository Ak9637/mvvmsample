package com.example.sample.mvvm.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Switch
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.sample.mvvm.R
import com.example.sample.mvvm.data.models.dataModels.User
import com.example.sample.mvvm.extensions.turnVisible
import com.facebook.drawee.view.SimpleDraweeView


class UsersAdapter(var users:List<User>) : RecyclerView.Adapter<UsersAdapter.MyViewHolder>() {

    private val clickListenerLiveData = MutableLiveData<User>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val mInflater = parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = mInflater.inflate(R.layout.users_item_row, parent, false)




        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return users.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = users[position]

        holder.nameTv.text = """name : ${item.name}"""
        holder.detailsTv.text = """number :${item.phone}"""
        holder.favSwitch.isChecked = item.fav

        holder.setHolderItemPostion(position)


    }


    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameTv = view.findViewById<TextView>(R.id.usersItemNameTv)
        val detailsTv = view.findViewById<TextView>(R.id.usersItemDetailsTv)
        val favSwitch = view.findViewById<Switch>(R.id.usersItemFavSwitch)
        val listener: CompoundButton.OnCheckedChangeListener =
            CompoundButton.OnCheckedChangeListener { _, isChecked ->run {
                users[currentPostion].fav = !users[currentPostion].fav
               // this@UsersAdapter.notifyItemChanged(currentPostion)
            }}
        val clickListener : View.OnClickListener=View.OnClickListener{
            clickListenerLiveData.value = users[currentPostion]
        }

        var currentPostion=0

        fun setHolderItemPostion(position:Int){
            this.currentPostion=position
            favSwitch.setOnCheckedChangeListener(listener)
            nameTv.setOnClickListener(clickListener)
            detailsTv.setOnClickListener (clickListener)
        }






    }

    fun updateData(newlist:List<User>){
        this.users=newlist
        notifyDataSetChanged()
    }

    fun getClickListener():LiveData<User>{
        return clickListenerLiveData
    }

}