package com.example.sample.mvvm.data

import com.example.sample.mvvm.data.models.EndPoints
import com.example.sample.mvvm.data.models.dataModels.User
import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService{

    @GET(EndPoints.GET_USERS)
    fun getUsers():Observable<Response<ArrayList<User>>>

}