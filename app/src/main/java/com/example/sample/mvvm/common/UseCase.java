package com.example.sample.mvvm.common;

public class UseCase<T> {
    private Boolean operationIsRunning=true;
    private T data;
    private String error="";
    private String failure="";
    private boolean isError=true;
    private boolean isFailure=false;
    private boolean isSuccess=false;




    public UseCase<T> startOperation(){
       operationIsRunning=true;
       return this;
    }
    public UseCase<T> dismissLoading(){
        operationIsRunning=false;
        return this;
    }


    public UseCase<T> setData(T data){
        this.isSuccess=true;
        this.isError=false;
        this.isFailure=false;
        this.operationIsRunning=false;
        this.data=data;

        return this;
    }

    public UseCase<T> setError(String error){
        this.isSuccess=false;
        this.isError=true;
        this.isFailure=false;
        this.error=error;
        this.operationIsRunning=false;
        return this;
    }
    public UseCase<T> setFailure(String failure){
        this.isSuccess=false;
        this.isError=false;
        this.isFailure=true;
        this.failure=failure;
        this.operationIsRunning=false;
        return this;
    }

    public UseCase<T> operationIsAlreadyInProgress(){
        this.operationIsRunning=false;
        return this;
    }

    public void setOperationIsRunning(Boolean operationIsRunning) {
        this.operationIsRunning = operationIsRunning;
    }

    public String getError() {
        return error;
    }

    public String getFailure() {
        return failure;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public boolean isFailure() {
        return isFailure;
    }

    public void setFailure(boolean failure) {
        isFailure = failure;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public T getData(){
        return data;
    }

    public Boolean getOperationIsRunning() {
        return operationIsRunning;
    }
    public Boolean checkOperationIsNotRunning() {
        return !operationIsRunning;
    }
}
