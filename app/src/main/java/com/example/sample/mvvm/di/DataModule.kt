package com.example.sample.mvvm.di


import com.example.sample.mvvm.data.local.SecureSharedPreferences
import com.example.sample.mvvm.helpers.SharedPreferencesManager
import com.example.sample.mvvm.root.ThisApplication
import dagger.Module
import dagger.Provides
import javax.inject.Named


/**
 *Module providing the Data Layer dependencies
 */
@Module
class DataModule {

    companion object {
        const val APP_PREFERENCES="App_Pref"
        val SECURED_PREF = "secured_pref"
        val SHARED_PREF = "my_pref"

    }



    @Provides
    @MyAppScope

    fun getSecuredSharedPreferenceHelper(context: ThisApplication): SecureSharedPreferences {
        return SecureSharedPreferences(SharedPreferencesManager(context, SECURED_PREF,true))
    }

    @Provides
    @MyAppScope
    @Named(APP_PREFERENCES)

    fun getSharedPreferenceHelper(context: ThisApplication): SharedPreferencesManager {
        return SharedPreferencesManager(context, SHARED_PREF,true)
    }



}