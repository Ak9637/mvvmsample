package com.example.sample.mvvm.di

import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

/**
 *Module providing retrofit dependencies
 */

@Module(includes = arrayOf(NetworkModule::class, HelperModule::class))
class RetrofitModule {
    @Provides
    @MyAppScope
    fun retrofit(gson: Gson, client: OkHttpClient,@Named("baseUrl") url:String): Retrofit {
        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .baseUrl(url)
                .build()

    }



}
