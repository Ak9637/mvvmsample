package com.example.sample.mvvm.extensions

import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import com.example.sample.mvvm.BuildConfig

fun Context.debugShort(message: String) {
    if (BuildConfig.DEBUG) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}

fun Context.debugLong(message: String) {
    if (BuildConfig.DEBUG) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}

fun Context.shortT(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.longT(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Context.getMyColor(resId:Int): Int {
    return try{
        ResourcesCompat.getColor(resources,resId,null)

    }catch (e:Exception){
        0x000000

    }
}
fun Context.getMyDrawable(resId:Int): Drawable? {
    return try{
        ResourcesCompat.getDrawable(resources,resId,null)

    }catch (e:Exception){
        null
    }
}
fun Context.getMyInteger(resId:Int): Int? {
    return try{
        resources.getInteger(resId)

    }catch (e:Exception){
        null
    }
}
fun Context.getMyDimen(resId:Int): Float {
    return try{
        resources.getDimension(resId)

    }catch (e:Exception){
        0f
    }
}
fun Context.getMyIntDimen(resId:Int): Int {
    return try{
        resources.getDimension(resId).toInt()

    }catch (e:Exception){
        0
    }
}



