package com.example.sample.mvvm.fragment_screens.users

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.sample.mvvm.R
import com.example.sample.mvvm.adapter.UsersAdapter
import com.example.sample.mvvm.common.BaseFragment
import com.example.sample.mvvm.screens.main.MainContracts
import com.example.sample.mvvm.screens.map.MapsActivity
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.user_fragment.*
import javax.inject.Inject
import javax.inject.Named


class UsersFragment : BaseFragment() , UsersContracts.View{


    companion object {
        const val TAG="UsersFragment"
    }

    @Inject
    @field: Named(UsersContracts.TAG)
    lateinit var viewModel: UsersContracts.ViewModel

    lateinit var usersAdapter : UsersAdapter



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.user_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {

        usersRv.layoutManager = LinearLayoutManager(this.context)



    }

    override fun onDestroy() {

        super.onDestroy()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)

        super.onAttach(context)
        processArgument(arguments)
        inits()


    }

    private fun processArgument(arguments: Bundle?) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

    }

    private fun inits(){


        viewModel.getUsers().observe(this, Observer {
            Log.d(MainContracts.TAG,"Users loaded :"+it.size)

            usersAdapter = UsersAdapter(it)
            usersRv.adapter = usersAdapter

            usersAdapter.getClickListener().observe(this@UsersFragment, Observer {user->
                MapsActivity.launchForUser(this@UsersFragment.activity!!,user)
            })
        })


    }





    override fun onActivityCreated(savedInstanceState: Bundle?) {

        super.onActivityCreated(savedInstanceState)

    }



}


