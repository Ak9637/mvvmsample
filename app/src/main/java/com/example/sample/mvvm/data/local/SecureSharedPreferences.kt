package com.example.sample.mvvm.data.local

import com.example.sample.mvvm.helpers.SharedPreferencesManager

/**
 * Local storage for data that need to be secured
 * currently stored in open but encryption logics can be implemented here
 * Maintain session data
 */



class SecureSharedPreferences(val pref: SharedPreferencesManager){

    companion object {
        const val TOKEN="token"
    }

    init {

        //saveToken("175c5b96-2be8-49d2-a41b-681750787116")

    }

    private var currentToken=""

    fun putData(key:String,value:Any){
            pref.put(key,value)
    }

    fun put(key:String,value:Any){
        when (value) {
            is String -> pref.putString(key,value)
            is Float -> pref.putFloat(key,value)
            is Int -> pref.putInt(key,value)
            is Boolean -> pref.putBoolean(key,value)
            is Long -> pref.putLong(key,value)
        }
        pref.forceCommit()
    }




    fun getString(key:String):String{
        return pref.getString(key,"")
    }
    fun getInt(key:String):Int{
        return pref.getInt(key,-1)
    }
    fun getFloat(key:String):Float{
        return pref.getFloat(key,0.0f)
    }
    fun getLong(key:String):Long{
        return pref.getLong(key,0)
    }
    fun getBoolean(key:String):Boolean{
        return pref.getBoolean(key,false)
    }

    fun getString(key:String,defaultValue:String):String{
        return pref.getString(key,defaultValue)
    }
    fun getInt(key:String,defaultValue:Int):Int{
        return pref.getInt(key,defaultValue)
    }
    fun getFloat(key:String,defaultValue:Float):Float{
        return pref.getFloat(key,defaultValue)
    }
    fun getLong(key:String,defaultValue: Long):Long{
        return pref.getLong(key,defaultValue)
    }
    fun getBoolean(key:String,defaultValue:Boolean):Boolean{
        return pref.getBoolean(key,defaultValue)
    }

    fun getToken(): String {
        if(currentToken.isEmpty()){
            currentToken=getString(TOKEN)
        }
        return currentToken

    }
    fun saveToken(newToken:String){
        currentToken=newToken
        putData(TOKEN,newToken)
    }

    fun isSessionActive():Boolean{
        return getToken()?.isNotEmpty()
    }

    fun logout(){
        saveToken("")
        currentToken=""

    }



}